SELECT * FROM permiso

INSERT INTO permiso (id, nombre) VALUES(1, 'Usuario_sel');
INSERT INTO permiso (id, nombre) VALUES(2, 'Usuario_ins');
INSERT INTO permiso (id, nombre) VALUES(3, 'Usuario_upd');
INSERT INTO permiso (id, nombre) VALUES(4, 'Usuario_del');

INSERT INTO permiso (id, nombre) VALUES(5, 'Cliente_sel');
INSERT INTO permiso (id, nombre) VALUES(6, 'Cliente_ins');
INSERT INTO permiso (id, nombre) VALUES(7, 'Cliente_upd');
INSERT INTO permiso (id, nombre) VALUES(8, 'Cliente_del');

INSERT INTO rol(id, nombre) VALUES (9, 'Admin');
INSERT INTO rol(id, nombre) VALUES (10, 'Auditor');

INSERT INTO rolpermiso(id, rol_id, permiso_id) VALUES(11, 9, 1);
INSERT INTO rolpermiso(id, rol_id, permiso_id) VALUES(12, 9, 2);
INSERT INTO rolpermiso(id, rol_id, permiso_id) VALUES(13, 9, 3);
INSERT INTO rolpermiso(id, rol_id, permiso_id) VALUES(14, 9, 4);

SELECT setval('hibernate_sequence', 14);

select * from hibernate_sequence 
