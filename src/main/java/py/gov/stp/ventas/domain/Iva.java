package py.gov.stp.ventas.domain;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.envers.Audited;
import org.hibernate.validator.constraints.NotBlank;
import py.gov.stp.core.domain.GenericEntity;

@Entity
@Audited
@Table(uniqueConstraints = @UniqueConstraint(name = "iva_descripcion_uk", columnNames = "descripcion"))
public class Iva extends GenericEntity {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	@NotBlank()
	@Size(max = 50)
	private String descripcion;
	@NotNull
	private Float porcentaje;
	@NotNull
	private Float baseImponible;

	public String getDescripcion() {

		return descripcion;
	}

	public void setDescripcion(String descripcion) {

		this.descripcion = descripcion;
	}

	public Float getPorcentaje() {

		return porcentaje;
	}

	public void setPorcentaje(Float porcentaje) {

		this.porcentaje = porcentaje;
	}

	public Float getBaseImponible() {

		return baseImponible;
	}

	public void setBaseImponible(Float baseImponible) {

		this.baseImponible = baseImponible;
	}

}
