package py.gov.stp.ventas.domain;

import java.math.BigDecimal;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.envers.Audited;
import org.hibernate.validator.constraints.NotBlank;
import py.gov.stp.core.domain.GenericEntity;

@Entity
@Audited
@Table(uniqueConstraints = @UniqueConstraint(name = "producto_descripcion_uk", columnNames = "descripcion"))
public class Producto extends GenericEntity {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@NotBlank(message = "{producto.descripcion.notBlank}")
	@Size(max = 100, message = "{producto.descripcion.size}")
	private String descripcion;

	@ManyToOne
	@NotNull(message = "{producto.categoria.notNull}")
	@JoinColumn(foreignKey = @ForeignKey(name = "producto_categoria_fk"))
	private Categoria categoria;

	private BigDecimal existencia;
	@NotNull(message = "{producto.precioVenta.notNull}")
	private BigDecimal precioVenta;
	@Size(max = 200, message = "{producto.imagen.size}")
	private String imagen;

	@ManyToOne
	@NotNull(message = "{producto.iva.notNull}")
	@JoinColumn(foreignKey = @ForeignKey(name = "producto_iva_fk"))
	private Iva iva;

	public String getDescripcion() {

		return descripcion;
	}

	public void setDescripcion(String descripcion) {

		this.descripcion = descripcion;
	}

	public Categoria getCategoria() {

		return categoria;
	}

	public void setCategoria(Categoria categoria) {

		this.categoria = categoria;
	}

	public BigDecimal getExistencia() {

		return existencia;
	}

	public void setExistencia(BigDecimal existencia) {

		this.existencia = existencia;
	}

	public BigDecimal getPrecioVenta() {

		return precioVenta;
	}

	public void setPrecioVenta(BigDecimal precioVenta) {

		this.precioVenta = precioVenta;
	}

	public String getImagen() {

		return imagen;
	}

	public void setImagen(String imagen) {

		this.imagen = imagen;
	}

	public Iva getIva() {

		return iva;
	}

	public void setIva(Iva iva) {

		this.iva = iva;
	}

	@Override
	public String toString() {

		return "Producto [descripcion=" + descripcion + ", categoria=" + categoria + "]";
	}

}
