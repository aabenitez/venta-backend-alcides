package py.gov.stp.ventas.domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.envers.Audited;

import com.fasterxml.jackson.annotation.JsonFormat;

import py.gov.stp.core.domain.GenericEntity;
import py.gov.stp.ventas.util.Cons;

@Entity
@Audited
@Table(uniqueConstraints = @UniqueConstraint(name = "venta_timbrado_nrodoc_uk", columnNames = { "timbrado_id",
		"nrodoc" }))
public class Venta extends GenericEntity {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@ManyToOne
	@NotNull(message = "{venta.cliente.notNull}")
	@JoinColumn(foreignKey = @ForeignKey(name = "venta_cliente_fk"))
	private Cliente cliente;

	@JsonFormat(pattern = Cons.TIMESTAMP_PATTERN)
	@Temporal(TemporalType.TIMESTAMP)
	@NotNull(message = "{venta.fechaVenta.notNull}")
	private Date fechaVenta;

	@ManyToOne
	@NotNull(message = "{venta.timbrado.notNull}")
	@JoinColumn(foreignKey = @ForeignKey(name = "venta_timbrado_fk"))
	private Timbrado timbrado;

	@NotNull(message = "{venta.cliente.notNull}")
	@Min(value = 1, message = "{venta.cliente.notNull}")
	private Integer nrodoc;

	private Boolean impreso;

	@Transient
	private List<DetalleVenta> detalleVentaList;

	public Cliente getCliente() {

		return cliente;
	}

	public void setCliente(Cliente cliente) {

		this.cliente = cliente;
	}

	public Date getFechaVenta() {

		return fechaVenta;
	}

	public void setFechaVenta(Date fechaVenta) {

		this.fechaVenta = fechaVenta;
	}

	public Timbrado getTimbrado() {

		return timbrado;
	}

	public void setTimbrado(Timbrado timbrado) {

		this.timbrado = timbrado;
	}

	public Integer getNrodoc() {

		return nrodoc;
	}

	public void setNrodoc(Integer nrodoc) {

		this.nrodoc = nrodoc;
	}

	public Boolean getImpreso() {

		return impreso;
	}

	public void setImpreso(Boolean impreso) {

		this.impreso = impreso;
	}

	public List<DetalleVenta> getDetalleVentaList() {
		if (detalleVentaList == null) {
			detalleVentaList = new ArrayList<>();
		}
		return detalleVentaList;
	}

	public void setDetalleVentaList(List<DetalleVenta> detalleVentaList) {
		this.detalleVentaList = detalleVentaList;
	}

}
