package py.gov.stp.ventas.domain;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.envers.Audited;

import com.fasterxml.jackson.annotation.JsonIgnore;

import py.gov.stp.core.domain.GenericEntity;

@Entity
@Audited
public class DetalleVenta extends GenericEntity {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@JsonIgnore
	@ManyToOne
	@NotNull(message = "{detalleVenta.venta.notNull}")
	@JoinColumn(foreignKey = @ForeignKey(name = "detalleVenta_venta_fk"))
	private Venta venta;

	@ManyToOne
	@NotNull(message = "{detalleVenta.productos.notNull}")
	@JoinColumn(foreignKey = @ForeignKey(name = "detalleVenta_venta_fk"))
	private Producto producto;

	@NotNull(message = "{detalleVenta.cantidad.notNull}")
	@Min(value = 1, message = "{detalleVenta.cantidad.notNull}")
	private BigDecimal cantidad;

	@NotNull(message = "{detalleVenta.precio.notNull}")
	private BigDecimal precio = BigDecimal.ZERO;
	private BigDecimal descuento;
	@NotNull(message = "{detalleVenta.precioTotal.notNull}")
	private BigDecimal precioTotal = BigDecimal.ZERO;
	private BigDecimal totalIva;

	@ManyToOne
	@NotNull(message = "{detalleVenta.productos.notNull}")
	@JoinColumn(foreignKey = @ForeignKey(name = "detalleVenta_venta_fk"))
	private Iva iva;

	private BigDecimal gravada;
	private BigDecimal exenta;

	public DetalleVenta() {
		// para pasar error de validación @Notnull
		iva = new Iva();
		venta = new Venta();
	}

	public Venta getVenta() {

		return venta;
	}

	public void setVenta(Venta venta) {

		this.venta = venta;
	}

	public Producto getProducto() {

		return producto;
	}

	public void setProducto(Producto producto) {

		this.producto = producto;
	}

	public BigDecimal getCantidad() {

		return cantidad;
	}

	public void setCantidad(BigDecimal cantidad) {

		this.cantidad = cantidad;
	}

	public BigDecimal getPrecio() {

		return precio;
	}

	public void setPrecio(BigDecimal precio) {

		this.precio = precio;
	}

	public BigDecimal getDescuento() {

		return descuento;
	}

	public void setDescuento(BigDecimal descuento) {

		this.descuento = descuento;
	}

	public BigDecimal getPrecioTotal() {

		return precioTotal;
	}

	public void setPrecioTotal(BigDecimal precioTotal) {

		this.precioTotal = precioTotal;
	}

	public BigDecimal getTotalIva() {

		return totalIva;
	}

	public void setTotalIva(BigDecimal totalIva) {

		this.totalIva = totalIva;
	}

	public Iva getIva() {

		return iva;
	}

	public void setIva(Iva iva) {

		this.iva = iva;
	}

	public BigDecimal getGravada() {

		return gravada;
	}

	public void setGravada(BigDecimal gravada) {

		this.gravada = gravada;
	}

	public BigDecimal getExenta() {

		return exenta;
	}

	public void setExenta(BigDecimal exenta) {

		this.exenta = exenta;
	}

	@Override
	public String toString() {
		return "DetalleVenta [producto=" + producto + ", cantidad=" + cantidad + ", precio=" + precio + ", precioTotal="
				+ precioTotal + ", totalIva=" + totalIva + "]";
	}

}
