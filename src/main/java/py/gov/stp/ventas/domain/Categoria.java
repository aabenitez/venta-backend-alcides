package py.gov.stp.ventas.domain;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Size;
import org.hibernate.envers.Audited;
import org.hibernate.validator.constraints.NotBlank;
import py.gov.stp.core.domain.GenericEntity;

@Entity
@Audited
@Table(uniqueConstraints = @UniqueConstraint(name = "categoria_descripcion_uk", columnNames = "descripcion"))
public class Categoria extends GenericEntity {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	@NotBlank(message = "{categoria.descripcion.notBlank}")
	@Size(max = 50, message = "{categoria.descripcion.size}")
	private String descripcion;

	public String getDescripcion() {

		return descripcion;
	}

	public void setDescripcion(String descripcion) {

		this.descripcion = descripcion;
	}

	@Override
	public String toString() {

		return "Categoria [descripcion=" + descripcion + "]";
	}

}
