package py.gov.stp.ventas.domain;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Size;
import org.hibernate.envers.Audited;
import org.hibernate.validator.constraints.NotBlank;
import py.gov.stp.core.domain.GenericEntity;

@Entity
@Audited
@Table(uniqueConstraints = @UniqueConstraint(name = "cliente_rucci_uk", columnNames = "rucci"))
public class Cliente extends GenericEntity {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@NotBlank(message = "{cliente.nombres.notBlank}")
	@Size(max = 100, message = "{cliente.nombres.size}")
	private String nombres;

	@NotBlank(message = "{cliente.apellidos.notBlank}")
	@Size(max = 100, message = "{cliente.apellidos.size}")
	private String apellidos;

	@NotBlank(message = "{cliente.rucCi.notBlank}")
	@Size(max = 20, message = "{cliente.rucCi.size}")
	private String rucCi;

	@NotBlank(message = "{cliente.telefono.notBlank}")
	@Size(max = 20, message = "{cliente.telefono.size}")
	private String telefono;

	public String getNombres() {

		return nombres;
	}

	public void setNombres(String nombres) {

		this.nombres = nombres;
	}

	public String getApellidos() {

		return apellidos;
	}

	public void setApellidos(String apellidos) {

		this.apellidos = apellidos;
	}

	public String getRucCi() {

		return rucCi;
	}

	public void setRucCi(String rucCi) {

		this.rucCi = rucCi;
	}

	public String getTelefono() {

		return telefono;
	}

	public void setTelefono(String telefono) {

		this.telefono = telefono;
	}

	@Override
	public String toString() {

		return "Cliente [nombres=" + nombres + ", apellidos=" + apellidos + ", rucCi=" + rucCi + "]";
	}

}
