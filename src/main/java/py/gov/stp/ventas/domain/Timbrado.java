package py.gov.stp.ventas.domain;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.hibernate.envers.Audited;

import com.fasterxml.jackson.annotation.JsonFormat;

import py.gov.stp.core.domain.GenericEntity;
import py.gov.stp.ventas.util.Cons;

@Entity
@Audited
@Table(uniqueConstraints = @UniqueConstraint(name = "timbrado_numero_uk", columnNames = "numero"))
public class Timbrado extends GenericEntity {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@NotNull(message = "{timbrado.numero.notNull}")
	private Integer numero;

	@NotNull(message = "{timbrado.numeroDesde.notNull}")
	private Integer numeroDesde;

	@NotNull(message = "{timbrado.numeroHasta.notNull}")
	private Integer numeroHasta;

	@JsonFormat(pattern = Cons.DATE_PATTERN)
	@Temporal(TemporalType.DATE)
	@NotNull(message = "{timbrado.fechaDesde.notNull}")
	private Date fechaDesde;

	@JsonFormat(pattern = Cons.DATE_PATTERN)
	@Temporal(TemporalType.DATE)
	@NotNull(message = "{timbrado.fechaHasta.notNull}")
	private Date fechaHasta;

	private Integer numeroActual;

	private Boolean activo;

	public Integer getNumero() {

		return numero;
	}

	public void setNumero(Integer numero) {

		this.numero = numero;
	}

	public Integer getNumeroDesde() {

		return numeroDesde;
	}

	public void setNumeroDesde(Integer numeroDesde) {

		this.numeroDesde = numeroDesde;
	}

	public Integer getNumeroHasta() {

		return numeroHasta;
	}

	public void setNumeroHasta(Integer numeroHasta) {

		this.numeroHasta = numeroHasta;
	}

	public Date getFechaDesde() {

		return fechaDesde;
	}

	public void setFechaDesde(Date fechaDesde) {

		this.fechaDesde = fechaDesde;
	}

	public Date getFechaHasta() {

		return fechaHasta;
	}

	public void setFechaHasta(Date fechaHasta) {

		this.fechaHasta = fechaHasta;
	}

	public Integer getNumeroActual() {

		return numeroActual;
	}

	public void setNumeroActual(Integer numeroActual) {

		this.numeroActual = numeroActual;
	}

	public Boolean getActivo() {

		return activo;
	}

	public void setActivo(Boolean activo) {

		this.activo = activo;
	}

	@Override
	public String toString() {

		return "Timbrado [numero=" + numero + ", fechaDesde=" + fechaDesde + ", fechaHasta=" + fechaHasta + "]";
	}

}
