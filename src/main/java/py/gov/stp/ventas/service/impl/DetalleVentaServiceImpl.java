package py.gov.stp.ventas.service.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.annotation.RequestScope;

import py.gov.stp.core.service.impl.GenericServiceImpl;
import py.gov.stp.ventas.domain.DetalleVenta;
import py.gov.stp.ventas.domain.Venta;
import py.gov.stp.ventas.repository.DetalleVentaRepository;
import py.gov.stp.ventas.service.DetalleVentaService;

@Service
@RequestScope
public class DetalleVentaServiceImpl extends GenericServiceImpl<DetalleVenta> implements DetalleVentaService {

	@Autowired
	private DetalleVentaRepository detalleVentaRepository;

	@Override
	public DetalleVentaRepository getRepository() {

		return detalleVentaRepository;
	}

	@Override
	public List<DetalleVenta> findAllByVentaCab(Venta venta) {

		return detalleVentaRepository.findAllByVenta(venta);
	}

	@Override
	public DetalleVenta insert(DetalleVenta det) {
		BigDecimal precio = det.getProducto().getPrecioVenta();
		BigDecimal precioTotal = precio.multiply(det.getCantidad());
		det.setPrecioTotal(precioTotal);
		det.setPrecio(precio);
		det.setIva(det.getProducto().getIva());
		BigDecimal baseImponible = BigDecimal.valueOf(det.getIva().getBaseImponible());

		if (baseImponible.compareTo(BigDecimal.ZERO) == 1) {
			det.setTotalIva(det.getPrecioTotal().divide(baseImponible, RoundingMode.CEILING));
		} else {
			det.setTotalIva(BigDecimal.ZERO);
		}

		super.insert(det);
		return det;
	}

}
