package py.gov.stp.ventas.service;

import java.util.List;
import py.gov.stp.core.service.GenericService;
import py.gov.stp.ventas.domain.DetalleVenta;
import py.gov.stp.ventas.domain.Venta;

public interface DetalleVentaService extends GenericService<DetalleVenta> {

	List<DetalleVenta> findAllByVentaCab(Venta venta);
}
