package py.gov.stp.ventas.service.impl;

import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.annotation.RequestScope;

import py.gov.stp.core.exception.BusinessLogicException;
import py.gov.stp.core.service.impl.GenericServiceImpl;
import py.gov.stp.ventas.domain.DetalleVenta;
import py.gov.stp.ventas.domain.Timbrado;
import py.gov.stp.ventas.domain.Venta;
import py.gov.stp.ventas.repository.VentaRepository;
import py.gov.stp.ventas.service.DetalleVentaService;
import py.gov.stp.ventas.service.VentaService;

@Service
@RequestScope
public class VentaServiceImpl extends GenericServiceImpl<Venta> implements VentaService {

	@Autowired
	private VentaRepository ventaRepository;
	@Autowired
	private DetalleVentaService detalleVentaService;

	private Logger logger = Logger.getLogger(getClass().getName());

	@Override
	protected VentaRepository getRepository() {
		return ventaRepository;
	}

	@Override
	@Transactional
	public Venta insert(Venta venta) {
		checkTimbrado(venta);
		super.insert(venta);
		for (DetalleVenta det : venta.getDetalleVentaList()) {
			det.setVenta(venta);
			detalleVentaService.insert(det);
			logger.info("Detalle:" + det);
		}
		return venta;
	}

	private void checkTimbrado(Venta venta) {
		Timbrado timbrado = venta.getTimbrado(); // como ya tenemos converter se
													// usa directo
		if (venta.getNrodoc() > timbrado.getNumeroHasta() || venta.getNrodoc() < timbrado.getNumeroDesde()) {
			throw new BusinessLogicException("Timbrado fuera de rango");
		}

	}

}
