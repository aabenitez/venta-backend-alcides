package py.gov.stp.ventas.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.annotation.RequestScope;
import py.gov.stp.core.service.impl.GenericServiceImpl;
import py.gov.stp.ventas.domain.Producto;
import py.gov.stp.ventas.repository.ProductoRepository;
import py.gov.stp.ventas.service.ProductoService;

@Service
@RequestScope
public class ProductoServiceImpl extends GenericServiceImpl<Producto> implements ProductoService {

	@Autowired
	private ProductoRepository repository;

	@Override
	public ProductoRepository getRepository() {

		return repository;
	}
}
