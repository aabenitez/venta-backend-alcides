package py.gov.stp.ventas.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.annotation.RequestScope;
import py.gov.stp.core.service.impl.GenericServiceImpl;
import py.gov.stp.ventas.domain.Categoria;
import py.gov.stp.ventas.repository.CategoriaRepository;
import py.gov.stp.ventas.service.CategoriaService;

@Service
@RequestScope
public class CategoriaServiceImpl extends GenericServiceImpl<Categoria> implements CategoriaService {

	@Autowired
	private CategoriaRepository categoriaRepository;

	@Override
	public CategoriaRepository getRepository() {

		return categoriaRepository;
	}

}
