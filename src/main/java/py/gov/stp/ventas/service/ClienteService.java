package py.gov.stp.ventas.service;

import py.gov.stp.core.service.GenericService;
import py.gov.stp.ventas.domain.Cliente;

public interface ClienteService extends GenericService<Cliente> {

}
