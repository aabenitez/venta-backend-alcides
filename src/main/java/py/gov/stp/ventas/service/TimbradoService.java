package py.gov.stp.ventas.service;

import py.gov.stp.core.service.GenericService;
import py.gov.stp.ventas.domain.Timbrado;

public interface TimbradoService extends GenericService<Timbrado> {

}
