package py.gov.stp.ventas.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.annotation.RequestScope;
import py.gov.stp.core.service.impl.GenericServiceImpl;
import py.gov.stp.ventas.domain.Iva;
import py.gov.stp.ventas.repository.IvaRepository;
import py.gov.stp.ventas.service.IvaService;

@Service
@RequestScope
public class IvaServiceImpl extends GenericServiceImpl<Iva> implements IvaService {

	@Autowired
	private IvaRepository ivaRepository;

	@Override
	public IvaRepository getRepository() {

		return ivaRepository;
	}

}
