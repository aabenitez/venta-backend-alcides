package py.gov.stp.ventas.service;

import py.gov.stp.core.service.GenericService;
import py.gov.stp.ventas.domain.Categoria;

public interface CategoriaService extends GenericService<Categoria> {

}
