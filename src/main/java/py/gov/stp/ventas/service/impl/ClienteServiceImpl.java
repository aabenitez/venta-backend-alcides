package py.gov.stp.ventas.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.annotation.RequestScope;
import py.gov.stp.core.service.impl.GenericServiceImpl;
import py.gov.stp.ventas.domain.Cliente;
import py.gov.stp.ventas.repository.ClienteRepository;
import py.gov.stp.ventas.service.ClienteService;

@Service
@RequestScope
public class ClienteServiceImpl extends GenericServiceImpl<Cliente> implements ClienteService {

	@Autowired
	private ClienteRepository clienteRepo;

	public ClienteRepository getRepository() {

		return clienteRepo;
	}
}
