package py.gov.stp.ventas.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.annotation.RequestScope;
import py.gov.stp.core.service.impl.GenericServiceImpl;
import py.gov.stp.ventas.domain.Timbrado;
import py.gov.stp.ventas.repository.TimbradoRepository;
import py.gov.stp.ventas.service.TimbradoService;

@Service
@RequestScope
public class TimbradoServiceImpl extends GenericServiceImpl<Timbrado> implements TimbradoService {

	@Autowired
	private TimbradoRepository timbradoRepository;

	@Override
	public TimbradoRepository getRepository() {

		return timbradoRepository;
	}

}
