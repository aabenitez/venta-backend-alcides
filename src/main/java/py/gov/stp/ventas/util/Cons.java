package py.gov.stp.ventas.util;

public final class Cons {

	public static final String DATE_PATTERN = "dd/MM/yyyy";
	public static final String TIMESTAMP_PATTERN = "dd/MM/yyyy HH:mm:ss";
	public static final String TIME_PATTERN = "HH:mm:ss";

}
