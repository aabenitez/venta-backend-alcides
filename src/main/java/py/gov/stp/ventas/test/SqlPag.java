package py.gov.stp.ventas.test;

import java.sql.SQLException;

import py.gov.stp.ventas.domain.Cliente;
import py.una.cnc.lib.db.DataSourcePool;
import py.una.cnc.lib.db.dataprovider.QueryBuilder;
import py.una.cnc.lib.db.dataprovider.SQLToObject;

public class SqlPag {

	private DataSourcePool pool;

	public SqlPag() {
		pool = new DataSourcePool();
		pool.init();
	}

	public void list() throws SQLException {

		SQLToObject<Cliente> sqlTo = new SQLToObject<>(pool, Cliente.class);
		QueryBuilder qb = new QueryBuilder();
		qb.setSelectFromWhere("SELECT cedula AS codigo, nombre, apellido FROM alumno");
		qb.setLimit(10);
		qb.build();
		for (Cliente clie : sqlTo.createList("java:jboss/datasources/ECOVRgcalib", qb.getPagingQuery(),
				qb.getQueryParamList().toArray())) {
			System.out.println(clie);
		}

	}

	public static void main(String[] args) throws SQLException {

		SqlPag pag = new SqlPag();
		pag.list();
	}
}
