package py.gov.stp.ventas.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import py.gov.stp.ventas.domain.Categoria;

public interface CategoriaRepository extends JpaRepository<Categoria, Long> {

}
