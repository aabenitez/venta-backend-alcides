package py.gov.stp.ventas.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import py.gov.stp.ventas.domain.Timbrado;

public interface TimbradoRepository extends JpaRepository<Timbrado, Long> {

}
