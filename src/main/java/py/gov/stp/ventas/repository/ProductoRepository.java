package py.gov.stp.ventas.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import py.gov.stp.ventas.domain.Producto;

public interface ProductoRepository extends JpaRepository<Producto, Long> {

}
