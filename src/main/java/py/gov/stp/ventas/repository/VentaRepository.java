package py.gov.stp.ventas.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import py.gov.stp.ventas.domain.Venta;

public interface VentaRepository extends JpaRepository<Venta, Long> {

}