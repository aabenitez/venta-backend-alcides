package py.gov.stp.ventas.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import py.gov.stp.ventas.domain.Cliente;

public interface ClienteRepository extends JpaRepository<Cliente, Long> {

}
