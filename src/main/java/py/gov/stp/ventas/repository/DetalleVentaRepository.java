package py.gov.stp.ventas.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import py.gov.stp.ventas.domain.DetalleVenta;
import py.gov.stp.ventas.domain.Venta;

public interface DetalleVentaRepository extends JpaRepository<DetalleVenta, Long> {

	List<DetalleVenta> findAllByVenta(Venta venta);
}
