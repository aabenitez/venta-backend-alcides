package py.gov.stp.ventas.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;
import py.gov.stp.core.service.GenericService;
import py.gov.stp.ventas.domain.Iva;
import py.gov.stp.ventas.service.IvaService;
import py.gov.stp.webmvc.controller.GenericController;

@RestController
@RequestScope
@RequestMapping("iva")
public class IvaController extends GenericController<Iva> {

	@Autowired
	private IvaService ivaService;

	@Override
	public GenericService<Iva> getService() {

		return ivaService;
	}

}
