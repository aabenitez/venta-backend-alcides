package py.gov.stp.ventas.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;
import py.gov.stp.core.service.GenericService;
import py.gov.stp.ventas.domain.Timbrado;
import py.gov.stp.ventas.service.TimbradoService;
import py.gov.stp.webmvc.controller.GenericController;

@RestController
@RequestScope
@RequestMapping("timbrados")
public class TimbradoController extends GenericController<Timbrado> {

	@Autowired
	private TimbradoService timbradoService;

	@Override
	public GenericService<Timbrado> getService() {

		return timbradoService;
	}

}
