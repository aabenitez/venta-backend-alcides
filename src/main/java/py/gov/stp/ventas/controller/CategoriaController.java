package py.gov.stp.ventas.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;
import py.gov.stp.core.service.GenericService;
import py.gov.stp.ventas.domain.Categoria;
import py.gov.stp.ventas.service.CategoriaService;
import py.gov.stp.webmvc.controller.GenericController;

@RestController
@RequestScope
@RequestMapping("categorias")
public class CategoriaController extends GenericController<Categoria> {

	@Autowired
	private CategoriaService categoriaService;

	@Override
	public GenericService<Categoria> getService() {

		return categoriaService;
	}

}
