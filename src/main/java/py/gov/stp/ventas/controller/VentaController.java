package py.gov.stp.ventas.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.validation.SmartValidator;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

import py.gov.stp.core.exception.DetailsValidationException;
import py.gov.stp.core.service.GenericService;
import py.gov.stp.core.util.ApiResponse;
import py.gov.stp.core.validation.FieldError;
import py.gov.stp.ventas.domain.DetalleVenta;
import py.gov.stp.ventas.domain.Venta;
import py.gov.stp.ventas.service.VentaService;
import py.gov.stp.webmvc.controller.GenericController;

@RestController
@RequestScope
@RequestMapping("ventas")
public class VentaController extends GenericController<Venta> {

	@Autowired
	private VentaService ventaService;
	@Autowired
	@Qualifier("mvcValidator")
	private SmartValidator validator;

	@Override
	public GenericService<Venta> getService() {
		return ventaService;
	}

	@Override
	public ResponseEntity<Venta> create(Venta venta) {
		checkDetalles(venta);
		ventaService.insert(venta);
		return new ResponseEntity<>(venta, HttpStatus.CREATED);
	}

	private void checkDetalles(Venta venta) {
		List<ApiResponse<Object>> apiResponseList = new ArrayList<>();
		for (DetalleVenta det : venta.getDetalleVentaList()) {
			BindingResult bindingResult = new BeanPropertyBindingResult(det, "DetalleVenta");
			validator.validate(det, bindingResult);
			// validamos cada detalle y el resultado de la validación
			// guardamos en el binding result
			if (bindingResult.hasErrors()) {
				ApiResponse<Object> apiResponse = new ApiResponse<>();
				List<FieldError> errores = new ArrayList<>();
				for (org.springframework.validation.FieldError fieldError : bindingResult.getFieldErrors()) {
					FieldError fe = new FieldError();
					fe.setDefaultMessage(fieldError.getDefaultMessage());
					fe.setField(fieldError.getField());
					fe.setRejectedValue(fieldError.getRejectedValue());
					errores.add(fe);
				}
				apiResponse.setFieldErrors(errores);
				apiResponse.setData(det);
				apiResponseList.add(apiResponse);
				logger.error("Erroes de validación de detalle: {} ", bindingResult.getFieldErrors());
			}
		}

		if (!apiResponseList.isEmpty()) {
			throw new DetailsValidationException(apiResponseList);
		}
	}
}
