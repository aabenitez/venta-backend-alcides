package py.gov.stp.ventas.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;
import py.gov.stp.core.service.GenericService;
import py.gov.stp.ventas.domain.Cliente;
import py.gov.stp.ventas.service.ClienteService;
import py.gov.stp.webmvc.controller.GenericController;

@RestController
@RequestScope
@RequestMapping("clientes")
public class ClienteController extends GenericController<Cliente> {

	@Autowired
	private ClienteService clienteService;

	@Override
	public GenericService<Cliente> getService() {

		return clienteService;
	}

}
