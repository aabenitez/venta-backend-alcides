package py.gov.stp.ventas.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;
import py.gov.stp.core.service.GenericService;
import py.gov.stp.ventas.domain.Producto;
import py.gov.stp.ventas.service.ProductoService;
import py.gov.stp.webmvc.controller.GenericController;

@RestController
@RequestScope
@RequestMapping("productos")
public class ProductoController extends GenericController<Producto> {

	@Autowired
	private ProductoService service;

	@Override
	public GenericService<Producto> getService() {

		return service;
	}

}
