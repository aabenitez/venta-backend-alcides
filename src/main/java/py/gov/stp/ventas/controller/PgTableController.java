package py.gov.stp.ventas.controller;

import java.sql.SQLException;
import java.util.List;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;
import py.gov.stp.core.main.AppContainer;
import py.gov.stp.ventas.dto.PgTableDTO;
import py.una.cnc.lib.db.dataprovider.QueryBuilder;
import py.una.cnc.lib.db.dataprovider.SQLToObject;

@RestController
@RequestScope
public class PgTableController {

	private Logger logger = Logger.getLogger(getClass().getName());
	@Autowired
	private AppContainer app;

	/**
	 * <code>
	 *   http://localhost:8080/rep/tables/datatable
	 *   http://localhost:8080/rep/tables/datatable?sSearch=us
	 *   http://localhost:8080/rep/tables/datatable?iDisplayStart=0&iDisplayLength=50
	 *   </code>
	 */

	@GetMapping("tables/datatable")
	public ResponseEntity<List<PgTableDTO>> getList(
			@RequestParam(required = false, defaultValue = "0") Integer iDisplayStart,
			@RequestParam(required = false, defaultValue = "10") Integer iDisplayLength,
			@RequestParam(required = false) String sSearch) {

		QueryBuilder qb = new QueryBuilder();
		qb.setSelectFromWhere(app.getSqlSource().get("select_pg_tables"));
		qb.setsSearch(sSearch);
		qb.setOffset(iDisplayStart);
		qb.setLimit(iDisplayLength);
		qb.setOrderBy("tablename DESC");
		qb.setFilterableColumn("tablename||tableowner");
		qb.build();

		SQLToObject<PgTableDTO> sqlToObj = new SQLToObject<>(app.getDataSourcePool(), PgTableDTO.class);
		List<PgTableDTO> list;
		try {
			logger.info("SQL count: " + qb.getCountingQuery());
			logger.info("SQL: " + qb.getPagingQuery());
			logger.info("PARAMETROS: " + qb.getQueryParamList());
			list = sqlToObj.createList("reportesDS", qb.getPagingQuery(), qb.getQueryParamList().toArray());
			return ResponseEntity.ok(list);
		} catch (SQLException e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
