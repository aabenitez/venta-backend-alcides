package py.gov.stp.ventas.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import py.gov.stp.ventas.converter.CategoriaConverter;
import py.gov.stp.ventas.converter.ClienteConverter;
import py.gov.stp.ventas.converter.IvaConverter;
import py.gov.stp.ventas.converter.ProductoConverter;
import py.gov.stp.ventas.converter.TimbradoConverter;

@Configuration
@EnableWebMvc
@ComponentScan("py.gov.stp")
@Import(ReporteSecurityConfig.class)
public class ReporteConfig extends WebMvcConfigurerAdapter {

	@Autowired
	private ClienteConverter clienteConverter;
	@Autowired
	private ProductoConverter productoConverter;
	@Autowired
	private CategoriaConverter categoriaConverter;
	@Autowired
	private IvaConverter ivaConverter;
	@Autowired
	private TimbradoConverter timbradoConverter;

	@Bean(name = "messageSource")
	public ResourceBundleMessageSource getMessageSource() {

		ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
		messageSource.setBasename("Messages");
		messageSource.setDefaultEncoding("UTF-8");
		messageSource.setUseCodeAsDefaultMessage(true);
		return messageSource;
	}

	@Override
	public void addCorsMappings(CorsRegistry registry) {

		registry.addMapping("/**").allowedOrigins("*").allowedMethods("GET", "POST", "PUT", "DELETE");
	}

	@Bean(name = "multipartResolver")
	public CommonsMultipartResolver createMultipartResolver() {

		CommonsMultipartResolver resolver = new CommonsMultipartResolver();
		resolver.setDefaultEncoding("utf-8");
		return resolver;
	}

	@Override
	public void addFormatters(FormatterRegistry registry) {

		registry.addConverter(productoConverter);
		registry.addConverter(clienteConverter);
		registry.addConverter(categoriaConverter);
		registry.addConverter(ivaConverter);
		registry.addConverter(timbradoConverter);
	}
}
