package py.gov.stp.ventas.config;

import java.util.List;

import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;

import py.gov.stp.security.config.DefaultSecurityConfig;
import py.gov.stp.security.config.UrlSec;

@EnableWebSecurity
public class ReporteSecurityConfig extends DefaultSecurityConfig {

	@Override
	protected void addToUrlSecList(List<UrlSec> urlSecList) {

		// urlSecList.add(new UrlSec("/productos/**", "Producto"));
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {

		http.csrf().disable();
		super.configure(http);
	}

	@Override
	protected void setAntMatchers(
			ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry expression) {

		expression.antMatchers(HttpMethod.GET, "/inicio/**").hasAnyAuthority("app_login", "root");
	}
}
