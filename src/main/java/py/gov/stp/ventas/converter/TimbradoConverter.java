package py.gov.stp.ventas.converter;

import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import py.gov.stp.ventas.domain.Timbrado;
import py.gov.stp.ventas.repository.TimbradoRepository;

@Component
public class TimbradoConverter implements Converter<String, Timbrado> {

	@Autowired
	private TimbradoRepository repository;

	@Override
	public Timbrado convert(String source) {

		return repository.findOne(NumberUtils.toLong(source, -1));
	}

}
