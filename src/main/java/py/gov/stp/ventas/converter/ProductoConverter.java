package py.gov.stp.ventas.converter;

import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import py.gov.stp.ventas.domain.Producto;
import py.gov.stp.ventas.repository.ProductoRepository;

@Component
public class ProductoConverter implements Converter<String, Producto> {

	@Autowired
	private ProductoRepository repository;

	@Override
	public Producto convert(String source) {

		return repository.findOne(NumberUtils.toLong(source, -1));
	}

}
