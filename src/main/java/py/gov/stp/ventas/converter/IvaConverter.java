package py.gov.stp.ventas.converter;

import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import py.gov.stp.ventas.domain.Iva;
import py.gov.stp.ventas.repository.IvaRepository;

@Component
public class IvaConverter implements Converter<String, Iva> {

	@Autowired
	private IvaRepository repo;

	@Override
	public Iva convert(String id) {
		return repo.findOne(NumberUtils.toLong(id));
	}

}
