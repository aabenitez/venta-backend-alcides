package py.gov.stp.ventas.converter;

import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import py.gov.stp.ventas.domain.Cliente;
import py.gov.stp.ventas.repository.ClienteRepository;

@Component
public class ClienteConverter implements Converter<String, Cliente> {

	@Autowired
	private ClienteRepository repository;

	@Override
	public Cliente convert(String source) {

		return repository.findOne(NumberUtils.toLong(source, -1));
	}

}
