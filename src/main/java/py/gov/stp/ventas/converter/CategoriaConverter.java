package py.gov.stp.ventas.converter;

import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import py.gov.stp.ventas.domain.Categoria;
import py.gov.stp.ventas.repository.CategoriaRepository;

@Component
public class CategoriaConverter implements Converter<String, Categoria> {

	@Autowired
	private CategoriaRepository repo;

	@Override
	public Categoria convert(String id) {

		return repo.findOne(NumberUtils.toLong(id));
	}

}
